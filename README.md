# surgeConfig
### Surge3 Custom Managed Configuration

> 使用时, 请在 iCloud 云盘 Surge 文件夹中建立 proxy 文件夹，加入 auto.list 和 all.list，详见样例
> proxy.list 定义可手动选择的代理（配置所有代理服务器，可手动切换）
> auto.list 定义自动测速切换的代理（配置需要自动测速的代理，url-test 和 fallback 使用）

### macOs 下文件存放位置

- 远程加载的规则集等文件缓存
> ~/Library/Caches/com.nssurge.surge-mac/External\ Resource

- GeoIP 数据库文件（MaxMind 创建的 GeoLite2 数据库）
> ~/Library/Application\ Support/com.nssurge.surge-mac/GeoLite2-Country.mmdb

- 主程序配置文件
> ~/Library/Application\ Support/com.nssurge.surge-mac/KDDefaults.plist

- 默认配置存放路径
> ~/Documents/Surge

- iCloud Drive（与 Surge iOS 版共享配置）
> ~/Library/Mobile\ Documents/iCloud\~com\~nssurge\~inc/Documents

- 设备名称映射
> ~/Library/Application\ Support/com.nssurge.surge-mac/deviceNameMap.json

- 设置网络服务代理的助手程序
> /Library/PrivilegedHelperTools/com.nssurge.surge-mac.helper

- 助手程序 LaunchDaemons
> /Library/LaunchDaemons/com.nssurge.surge-mac.helper.plist
